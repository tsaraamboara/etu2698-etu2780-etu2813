<?php
    include("fonction.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        form {
            max-width: 300px;
            margin: 50px auto;
            background: #ffffff; /* White background */
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        input[<style>]
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        form {
            max-width: 300px;
            margin: 50px auto;
            background: #ffffff; /* White background */
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type="button"] {
            background-color: #4caf50; /* Green background */
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type="button"]:hover {
            background-color: #45a049; /* Darker green on hover */
        }

        h1 {
            text-align: center;
            color: #4caf50; /* Green color for heading */
        }

        footer {
            text-align: center;
            margin-top: 20px;
        }
    </style>
<script type="text/javascript">
function getMaxWeight() {
    var xhr;
    try {
        xhr = new ActiveXObject('Msxml2.XMLHTTP');
    } catch (e) {
        try {
            xhr = new ActiveXObject('Microsoft.XMLHTTP');
        } catch (e2) {
            try {
                xhr = new XMLHttpRequest();
            } catch (e3) {
                xhr = false;
            }
        }
    }

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                var valiny = JSON.parse(xhr.responseText);
                console.log(xhr.responseText);
                var maxWeight = valiny.nombre_pieds; // Récupérer la valeur maximale du poids à partir de l'objet mysqli_result
                console.log(maxWeight);
                // Utiliser la valeur reçue dans la vérification du poids
                // valider(maxWeight);
                // Utiliser la valeur reçue dans la vérification du poids
            } else {
                document.dyn = "Error code " + xhr.status;
            }
        }
    };
    var idParcelle = document.getElementsByName("choixParcelle")[0].value;
    console.log(idParcelle);
    xhr.open("GET", "getMaxWeight.php?choixParcelle=" + idParcelle, true);
    xhr.send(null);
}

function valider(maxWeight) {
    // Récupérer la valeur du champ "poidsCueilli"
    var poidsCueilli = document.getElementsByName("poidsCueilli")[0].value;

    // Vérifier si la valeur est inférieure à la valeur maximale
    if (poidsCueilli < maxWeight) {
        // Si la valeur est inférieure à la valeur maximale, alors soumettre le formulaire
        document.forms[0].submit();
    } else {
        // Sinon, afficher un message d'erreur
        alert("Le poids cueilli doit être inférieur à " + maxWeight);
    }
}

     
</script>
</head>
<body>
    <h1>Thé machas</h1>
    <form action="traitementCueillette.php" method="post">
        <input type="date" placeholder="date" name="daty">
        <select name="choixCueilleur" id="choixCueilleur">
        <?php
            $valiny=listeCueilleur();
            while($a=mysqli_fetch_assoc($valiny))
            { ?>
            <option value="<?php echo $a['idCueilleur']; ?>"> <?php echo $a['nomCueilleur'] ?></option>
        <?php } ?>
        </select>

        <select name="choixParcelle" id="choixParcelle">
        <?php
            $vali=listeParcelle();
            while($b=mysqli_fetch_assoc($vali))
            { ?>
            <option value="<?php echo $b['idParcelle']; ?>"> <?php echo $b['surface'] ?> m²</option>
        <?php } ?>
        </select>
        <input type="number" placeholder="poids" name="poidsCueilli">
        <input type="submit" value="Cueillir" >
    </form>
    <a href="resultat.php">suivant</a>
    <footer>
        &copy; Liane:ETU2698 - Amboara:ETU2780 - Andry:ETU2813
    </footer>
</body>
</html>


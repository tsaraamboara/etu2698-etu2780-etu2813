<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <title>log in</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-image:url('tea.jpg');
            margin: 0;
            padding: 0;
        }

        form {
            max-width: 300px;
            margin: 50px auto;
            background: #333;/* Gray background */
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type="submit"] {
            background-color: #4caf50; /* Green background */
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #45a049; /* Darker green on hover */
        }

        h1 {
            text-align: center;
            color: #4caf50; /* Green color for heading */
        }

        footer {
            text-align: center;
            position: fixed; /* Fixed position to stick to bottom */
            left: 0;
            bottom: 0;
            width: 100%; /* Full width */
            background: #333; /* White background */
            padding: 20px 0; /* Adjust padding as needed */
            color: black; /* Change footer text color */
        }

    </style>
</head>
<body class="container col-md-offset-3 col-md-3 col-md-offset-6">
    <h1>The Sahambavy</h1>
    <form action="traitementIndex.php" method="post" >
        <input type="text" value="john@example.com" name="nom">
        <input type="password" value="password1" name="pwd">
        <input type="submit" value="Sign in">
    </form>

    <form action="traitementIndex.php" method="post" >
        <input type="text" value="admin@example.com" name="nom">
        <input type="password" value="admin123" name="pwd">
        <input type="submit" value="Sign in">
    </form>
    
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    
    <footer>
        &copy; Liane:ETU2698 - Amboara:ETU2780 - Andry:ETU2813
    </footer>
</body>
</html>

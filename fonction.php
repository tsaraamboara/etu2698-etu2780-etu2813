<?php
function dbconnect()
{
    static $connect = null;
    if ($connect === null) {
        $connect = mysqli_connect('172.70.2.135', 'ETU002780', 'cNEceT63N7HI', 'db_desp3_ETU002780');
    }
    return $connect;
}
function verifConnex($email,$pswd)
{ 
    $sql="select * from user_the where email='%s' and mdp='%s'";
    $sql= sprintf($sql,$email,$pswd);
    $creation = mysqli_query(dbconnect(),$sql);
    $anarana="admin@example.com";
    $truc=mysqli_num_rows($creation);
    if($truc==0)
    {
        header('location:index.php');
    }
    else
    {
        if($email==$anarana)
        {
            header('location:accueilAdmin.php');
        }
        else
        {
            header('location:home.php');
        }
    }
    mysqli_free_result($creation);
}
function listeCueilleur()
{ 
    $sql="select * from cueilleur_the";
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function listeParcelle()
{ 
    $sql="select * from parcelle_the";
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function insertionCueillette($poids,$daty,$idCueiller,$idParcelle)
{ 
    $sql="INSERT INTO cueillette_the ( poids,daty,idCueilleur,idParcelle)  VALUES (%d,'%s',%d,%d)";
    $sql= sprintf($sql,$poids,$daty,$idCueiller,$idParcelle);
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function insertDepense($daty,$idCategorie,$montant)
{ 
    $sql="INSERT INTO depense_the (daty,idCategorie,montant)  VALUES ('%s',%d,%d)";
    $sql= sprintf($sql,$daty,$idCategorie,$montant);
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function TotalDepense()
{ 
    $sql="select sum(montant) from depenses_the";
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function TotalSalaire()
{ 
    $sql="SELECT SUM(cu.poids * s.montantSalaire) AS salaire_total
FROM cueilleur_the c
JOIN cueillette_the cu ON c.idCueilleur = cu.idCueilleur
JOIN parcelle_the p ON cu.idParcelle = p.idParcelle
JOIN salaire_the s ON c.idCueilleur = s.idSalaire";
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function nbPieds($idParcelle)
{
    $sql="SELECT ROUND((p.surface / v.occupation)) AS nombre_pieds
    FROM parcelle_the p
    JOIN variete_the v ON p.idVariete = v.idVariete where idparcelle=%d";
            $sql= sprintf($sql,$idParcelle);
        $creation = mysqli_query(dbconnect(),$sql);
        return $creation;
}
function poids($nbpieds,$idParcelle)
{
    $sql="SELECT ( %d * v.rendement) AS nombre_pieds
    FROM parcelle_the p
    JOIN variete_the v ON p.idVariete = v.idVariete where idparcelle=%d";
            $sql= sprintf($sql,$nbpieds,$idParcelle);
        $creation = mysqli_query(dbconnect(),$sql);
        return $creation;
}
function listeCategorie()
{ 
    $sql="select * from categorieDepense_the";
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function insertionVariete($nomVariete ,$occupation,$rendement)
{ 
    $sql="INSERT INTO variete_the (nomVariete ,occupation,rendement)  VALUES ('%s',%d,%d)";
    $sql= sprintf($sql,$nomVariete ,$occupation,$rendement);
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function listeVariete()
{ 
    $sql="select * from variete_the";
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function insertionParcelle($surface ,$idVariete)
{ 
    $sql="INSERT INTO parcelle_the (surface ,idVariete)  VALUES (%d,%d)";
    $sql= sprintf($sql,$surface ,$idVariete);
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function insertionCueilleur_the($nomCueilleur,$genreCeuilleur,$dtn,$salaire)
{ 
    $sql="INSERT INTO cueilleur_the (nomCueilleur,genreCeuilleur,dtn,salaire)  VALUES ('%s','%s','%s',%d)";
    $sql= sprintf($sql,$nomCueilleur,$genreCeuilleur,$dtn,$salaire);
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function insertionCategorieDepense_the($nomCategorie)
{ 
    $sql="INSERT INTO categorieDepense_the (nomCategorie)  VALUES ('%s')";
    $sql= sprintf($sql,$nomCategorie);
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function updateMontant($vola)
{ 
    $sql="UPDATE cueilleur_the SET salaire = %d";
    $sql= sprintf($sql,$vola);
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
function differenceMois($dateDebut, $dateFin) {
    $dateDebutObj = new DateTime($dateDebut);
    $dateFinObj = new DateTime($dateFin);
    
    $moisDebut = $dateDebutObj->format('n');
    $moisFin = $dateFinObj->format('n');
    
    $differenceMois = abs($moisFin - $moisDebut);
    
    return $differenceMois ;
}
function afficheResultat($dateDebut, $dateFin)
{ 
    //am affichage reste+regeneration no atao satria miampy ilay izy isaky ny mois vaovao
    $truc= differenceMois($dateDebut, $dateFin);
    $sql="select poids_total_cueilli,poids-poids_total_cueilli as restes,poids*%d as regenerations,salaire_total/poids_total_cueilli as revenu from quantithe where daty BETWEEN '%s' and '%s'";
    $sql= sprintf($sql,$truc,$dateDebut, $dateFin);
    $creation = mysqli_query(dbconnect(),$sql);
    return $creation;
}
?>
create DATABASE the;
use the;
create table variete_the(
    idVariete int PRIMARY KEY AUTO_INCREMENT,
    nomVariete VARCHAR(10),
    occupation DOUBLE,
    rendement DOUBLE
);
CREATE TABLE parcelle_the (
    idParcelle INT PRIMARY KEY AUTO_INCREMENT,
    surface DOUBLE,
    idVariete INT,
    FOREIGN KEY (idVariete) REFERENCES variete_the(idVariete)
);
CREATE Table cueilleur_the(
    idCueilleur INT PRIMARY KEY AUTO_INCREMENT,
    nomCueilleur VARCHAR(20),
    genreCeuilleur CHAR,
    dtn DATE,
    salaire DOUBLE
);
CREATE Table categorieDepense_the(
    idCategorieDepense INT PRIMARY KEY AUTO_INCREMENT,
    nomCategorie VARCHAR(20)
);
CREATE Table cueillette_the(
    idCueillette INT PRIMARY KEY AUTO_INCREMENT,
    poids double,
    daty DATE,
    idCueilleur INT,
    idParcelle INT,
   FOREIGN KEY (idCueilleur) REFERENCES cueilleur_the(idCueilleur),
   FOREIGN KEY (idParcelle) REFERENCES parcelle_the(idParcelle)
);

CREATE table user_the(
idUser int PRIMARY KEY AUTO_INCREMENT,
email VARCHAR(20),
mdp VARCHAR(20)
);
create table depense_the(
    idDepense INT PRIMARY KEY AUTO_INCREMENT,
    daty DATE,
    idCategorie int,
    montant DOUBLE,
    FOREIGN KEY (idCategorie) REFERENCES categorieDepense_the(idCategorieDepense)
);
create table salaire_the(
    idSalaire INT PRIMARY KEY AUTO_INCREMENT,
    montantSalaire DOUBLE
);

/*données de test*/
INSERT INTO variete_the (idVariete, nomVariete, occupation, rendement)
VALUES
(1, 'The vert', 10.5, 20.3),
(2, 'The rouge', 15.2, 25.8);


INSERT INTO cueilleur_the (idCueilleur, nomCueilleur, genreCeuilleur, dtn, salaire)
VALUES
(1, 'Jean Dupont', 'M', '1990-05-15', 1200.50),
(2, 'Marie Durand', 'F', '1995-07-20', 1100.75),
(3, 'Pierre Martin', 'M', '1988-12-10', 1300.25);
INSERT INTO categorieDepense_the (idCategorieDepense, nomCategorie)
VALUES
(1, 'Semences'),
(2, 'Engrais'),
(3, 'Pesticides');

INSERT INTO user_the (idUser, email, mdp)
VALUES
(1, 'john@example.com', 'password1'),
(2, 'jane@example.com', 'password2'),
(3, 'admin@example.com', 'admin123');
INSERT INTO depense_the (idDepense, daty, idCategorie, montant)
VALUES
(1, '2023-05-10', 1, 500.75),
(2, '2023-05-12', 2, 300.25),
(3, '2023-05-15', 3, 400.50);
INSERT INTO salaire_the (idSalaire, montantSalaire)
VALUES
(1, 2500.50),
(2, 2600.75),
(3, 2700.25);
INSERT INTO cueillette_the (idCueillette, poids, daty, idCueilleur, idParcelle)
VALUES
(1, 50.2, '2023-05-10', 1, 1),
(2, 45.7, '2023-05-12', 2, 2),
(3, 55.9, '2023-05-15', 3, 3);
INSERT INTO parcelle_the (idParcelle, surface, idVariete)
VALUES
(1, 100.5, 1),
(2, 85.3, 2),
(3, 120.8, 1);

CREATE or replace view quantithe as 
select p.idParcelle,
p.idVariete,ROUND((p.surface / v.occupation)) as pieds,
 ROUND((p.surface / v.occupation))*rendement as poids,
  SUM(c.poids) AS poids_total_cueilli ,
  c.daty,SUM(cu.poids * s.montantSalaire) AS salaire_total 
  from parcelle_the p 
  join variete_the v on p.idVariete=v.idVariete
   JOIN cueillette_the c ON p.idParcelle = c.idParcelle 
   JOIN  cueillette_the cu ON c.idCueilleur = cu.idCueilleur
    JOIN salaire_the s ON c.idCueilleur = s.idSalaire
GROUP BY p.idParcelle, p.surface,daty,cu.idCueillette;
select * from parcelle_the;



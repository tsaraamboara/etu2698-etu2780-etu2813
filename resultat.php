<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Resultat</title>
</head>
<style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        form {
            max-width: 300px;
            margin: 50px auto;
            background: #ffffff; /* White background */
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type="button"] {
            background-color: #4caf50; /* Green background */
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type="button"]:hover {
            background-color: #45a049; /* Darker green on hover */
        }

        h1 {
            text-align: center;
            color: #4caf50; /* Green color for heading */
        }

        footer {
            text-align: center;
            position: fixed; /* Fixed position to stick to bottom */
            left: 0;
            bottom: 0;
            width: 100%; /* Full width */
            background: #333; /* White background */
            padding: 20px 0; /* Adjust padding as needed */
            color: black; /* Change footer text color */
        }
    </style>
</head>
<body>
    <h1>Resultat de vos requête</h1>
    <form action="traitementResultat.php" method="post">
        <p>Date début<input type="date" name="dtDebut" id="dtDebut"></p>
        <p>Date Fin<input type="date" name="dtFin" id="dtFin"></p>
        <button type="submit">voir</button>
    <!-- </form>
    <p>Poids total cueillette </p>
    
    <p>Poids restant</p>
    <p>coût de revient par kg</p> -->
</body>
<footer>
        &copy; Liane:ETU2698 - Amboara:ETU2780 - Andry:ETU2813
</footer>
</html>
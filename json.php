<?php
include("fonction.php");

$dateFin = $_GET['truc'];

$dateDebut = '2023-01-01';

$valiny = afficheResultat($dateDebut, $dateFin);

$reponse = array();

while ($a = mysqli_fetch_assoc($valiny)) {
    $reponse['poids_total_cueilli'] = $a['poids_total_cueilli'];
    $reponse['regenerations'] = $a['regenerations'] + $a['restes'];
    $reponse['revenu'] = $a['revenu'];
}

mysqli_free_result($valiny);

echo json_encode($reponse);
?>
